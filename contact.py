#!/usr/bin/env python
# -*- coding: UTF8 -*-

class Contact:
	"Classe Contact"

	def __init__(self,uneAdresse,unNom="",unPrenom=""):
		self.adresse = uneAdresse
		self.nom = unNom
		self.prenom = unPrenom

	def getNom(self):
		return self.nom

	def setNom(self, unNom):
		self.nom = unNom

	def getPrenom(self):
		return self.prenom

	def setPrenom(self, unPrenom):
		self.prenom = unPrenom

	def getAdresse(self):
		return self.adresse

	def setAdresse(self, uneAdresse):
		self.adresse = uneAdresse

	def affiche(self):
		print "\nNom\t: ",self.nom
		print "\nPrénom\t: ",self.prenom
		print "\nAdresse\t: ",self.adresse

	def getList(self):
		return [self.nom,self.prenom,self.infos,self.adresse]

# Programme de test :
if __name__ == '__main__':
	c1 = Contact("tux@localhost","Tux","Dd")
	c1.affiche()
