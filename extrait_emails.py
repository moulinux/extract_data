#!/usr/bin/env python
# -*- coding: UTF8 -*-

# http://www.chicoree.fr/w/Fichiers_CSV_en_Python

import csv
from contact import Contact

def lireCsv (unFichier):
	#
	# Ouverture du fichier source.
	#
	# D'après la documentation, le mode ''b'' est
	# *obligatoire* sur les plate-formes où il est
	# significatif. Dans la pratique, il est conseillé
	# de toujours le mettre.
	#
	monFichier=open(unFichier, "rb")
	mesContacts=[]

	try:
		#
		# Création des ''lecteurs'' CSV.
		#
		monLecteur = csv.reader(monFichier)
	
		#
		# Le ''lecteur'' est itérable, et peut être utilisé
		# dans une boucle ''for'' pour extraire les
		# lignes une par une.
		#
		for ligne in monLecteur:
			if ligne[2] != '' :
				print ligne[0].decode("utf-8"),"\t",
				print ligne[1].decode("utf-8"),"\t",
				print ligne[2]
				monContact = Contact(ligne[2],ligne[0].decode('Utf8'),ligne[1].decode('Utf8'))
				mesContacts.append(monContact)
	finally:
		#
		# Fermeture du fichier source
		#
		monFichier.close()

	return mesContacts


def afficheChEmails(lesContacts):
	for unContact in lesContacts:
		print unContact.getAdresse(),", ",


print "\n",'*'*10," Lecture du fichier ",'*'*10
nomFichier="fic/donnees.csv"
lesContacts=lireCsv(nomFichier)

print "\n",'*'*10," Affichage des emails",'*'*10
afficheChEmails(lesContacts)
